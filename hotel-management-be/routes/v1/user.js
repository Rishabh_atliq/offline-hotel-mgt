const express = require("express");
const { userLogin } = require("../../controllers/user");
let router = express.Router();
const { CommonRoutes } = require("../../services/common-routes");

router = CommonRoutes(router, "tbl_user");
router.post('/userlogin', userLogin)
router.post('/userRegistration')
module.exports = router;
