const express = require("express");
const { getUserList } = require("../../controllers/user");
let router = express.Router();
const { CommonRoutes } = require("../../services/common-routes");

router = CommonRoutes(router, "tbl_hotel");

module.exports = router;
