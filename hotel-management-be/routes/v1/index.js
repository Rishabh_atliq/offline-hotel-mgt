const express = require("express");
const router = express.Router();
const user = require("./user");
const hotel = require("./hotel");
const reservation = require("./reservation");
const room = require("./room");
const customer = require("./customer");
const role = require("./role");

router.use("/health-check", (_, res) =>
  res.send("Welcome to Hotel Management")
);
router.use("/user", user);
router.use("/hotel", hotel);
router.use("/reservation", reservation);
router.use("/room", room);
router.use("/customer", customer);
router.use("/role", role);

module.exports = router;
