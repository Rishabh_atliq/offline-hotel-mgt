var { Sequelize, Op } = require("sequelize");
require("dotenv").config();
const LocalDB = () => {
  let dbOPtion = {
    host: process.env.DBHOST || "localhost",
    dialect: "mysql",
    port: 3306,
    logging: false,

    pool: {
      max: 5,
      min: 0,
      idle: 10000,
    },
  };
  let password = process.env.DBPASSWORD || "";
  const user = process.env.DBUSER || "root";
  return new Sequelize(
    process.env.DBNAME || "hotel_management",
    user,
    password,
    dbOPtion
  );
};
const ServerDB = () => {
  let dbOPtion = {
    host: process.env.DBHOST || "localhost",
    dialect: "mysql",
    port: 3306,
    logging: false,

    pool: {
      max: 5,
      min: 0,
      idle: 10000,
    },
  };
  let password = process.env.DBPASSWORD || "";
  const user = process.env.DBUSER || "root";
  return new Sequelize("hotel_management1", user, password, dbOPtion);
};

module.exports = {
  LocalDB,
  ServerDB,
};
