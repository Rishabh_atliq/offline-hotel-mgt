const SYNCMETHODS = ["POST", "PUT", "PATCH", "DELETE"];
module.exports = {
  SYNCMETHODS,
};
