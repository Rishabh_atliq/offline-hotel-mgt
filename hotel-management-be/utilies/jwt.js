var jwt = require("jsonwebtoken");

const privateKey = "ThisIsWeBTOken";

const getToken = (user_id) => {
  return jwt.sign({ user_id }, privateKey, { expiresIn: 43200 });
};
const verifyToken = (token) => {
  return new Promise((res, rej) => {
    jwt.verify(token, privateKey, (err, decoded) => {
      if (err) {
        rej(err);
      } else {
        res(decoded);
      }
    });
  });
};

module.exports = {
  getToken,
  verifyToken,
};
