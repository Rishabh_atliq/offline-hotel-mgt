const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const path = require("path");
const app = express();
app.use(cors());
app.use(bodyParser.json());
const createServer = (port, baseRoute, routes, staticPath) => {
  baseRoute ? app.use(baseRoute, routes) : app.use(routes);
  const public = path.join(__dirname, staticPath);
  app.use(express.static(public));
  app.use("/", express.static(public));

  app.use("**", express.static(public));
  app.listen(port, () => console.log(`Server is running on port ${port}.`));
};

module.exports = {
  createServer,
};
