const { eventEmitter } = require("../services/events");
const { SYNCMETHODS } = require("./constant");
const { verifyToken } = require("./jwt");
// const axios = require("axios").default;
// const serverURL = "http://localhost:8080";
require("dotenv").config();
const isServer = process.env.SERVER;

const middleFunctionCall = async (
  fun,
  req,
  res,
  tableName,
  shouldAuth = false
) => {
  try {
    if (shouldAuth) {
      const user = await verifyToken(req.headers.authorization);
      req.userDetails = user;
    }
    let sync = true;
    let result;
    let forceSync = false;
    if (isServer === "true") {
    } else if (SYNCMETHODS.includes(req.method)) {
      try {
        await fun(req, res, tableName, sync, true);
        // eventEmitter.emit("serverDBOnline");
      } catch (e) {
        sync = false;
        if (e.name !== "SequelizeConnectionRefusedError") {
          forceSync = true;
        }
      }
    }
    if (isServer === "true") {
      result = await fun(req, res, tableName, sync, true);
      res.json(formatResponse(result));
    } else {
      result = await fun(req, res, tableName, sync);
      res.json(formatResponse(result));
    }
    if (forceSync) {
      eventEmitter.emit("serverDBOnline");
    }
  } catch (e) {
    console.log(e);
    if (e && e.name === "TokenExpiredError") {
      return res.status(401).json({
        success: false,
        error: e,
      });
    }
    res.status(501).json({
      success: false,
      error: e,
    });
  }
};

const middleFunctionCall1 = async (fun, req, res, shouldAuth = true) => {
  try {
    if (shouldAuth) {
      const user = await verifyToken(req.headers.authorization);
      req.userDetails = user;
    }
    let result = await fun(req, res);
    res.json(result);
  } catch (e) {
    if (e && e.name === "TokenExpiredError") {
      return res.status(401).json({
        success: false,
        error: e,
      });
    }
    res.status(501).json({
      success: false,
      error: e,
    });
  }
};

const dbObject = (isServer, serverDB, localDB) =>
  isServer ? serverDB : localDB;

const formatResponse = (data) => {
  return {
    success: true,
    data,
  };
};

module.exports = {
  middleFunctionCall,
  formatResponse,
  middleFunctionCall1,
  dbObject,
};
