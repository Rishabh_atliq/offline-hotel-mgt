const { createServer } = require("./utilies/app");
const routes = require("./routes");
createServer(process.env.PORT || 8080, "/api", routes, "../view");
