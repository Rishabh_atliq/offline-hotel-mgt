const { db, serverDB } = require("../models");
var { Op } = require("sequelize");
const sqEI = require("../utilies/sequelize-import-export");
const { dbObject } = require("../utilies/functions");
const { eventEmitter } = require("./events");
const path = require("path");
const dbFile = path.join(__dirname, "/databaseexport.sequelize");
eventEmitter.on("serverDBOnline", async () => {
  await exportDB();
});
const importDB = async () => {
  let dbex = new sqEI(Object.values(serverDB));
  await dbex.import(dbFile);
};
const exportDB = async () => {
  let dbex = new sqEI(Object.values(db));
  await dbex.export(dbFile);
  await importDB();
};
const create = async (req, res, tableName, is_sync, is_server) => {
  req.body.is_sync = is_sync;
  await dbObject(is_server, serverDB, db)[tableName].create(req.body);
};

const deleted = async (req, res, tableName, is_sync, is_server) => {
  req.body.is_sync = is_sync;
  return await dbObject(is_server, serverDB, db)[tableName].update(
    { is_active: false, is_delete: true },
    { where: { id: req.params.id } }
  );
};

const update = async (req, res, tableName, is_sync, is_server) => {
  req.body.is_sync = is_sync;
  return await dbObject(is_server, serverDB, db)[tableName].update(req.body, {
    where: { id: req.params.id },
  });
};

const findAll = async (req, res, tableName, is_sync, is_server) => {
  return await dbObject(is_server, serverDB, db)[tableName].findAll();
};

const findAllActive = async (req, res, tableName, is_sync, is_server) => {
  const where = {
    is_active: { [Op.eq]: true },
  };
  return await dbObject(is_server, serverDB, db)[tableName].findAll({ where });
};

module.exports = {
  findAll,
  findAllActive,
  create,
  deleted,
  update,
};
