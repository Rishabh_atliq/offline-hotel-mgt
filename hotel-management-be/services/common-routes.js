const { middleFunctionCall } = require("../utilies/functions");
const {
  create,
  deleted,
  findAll,
  findAllActive,
  update,
} = require("./database");

const CommonRoutes = (router, tableName) => {
  router.get("/", (req, res) =>
    middleFunctionCall(findAllActive, req, res, tableName)
  );
  router.get("/findAll", (req, res) =>
    middleFunctionCall(findAll, req, res, tableName)
  );
  router.delete("/:id", (req, res) =>
    middleFunctionCall(deleted, req, res, tableName)
  );
  router.put("/:id", (req, res) =>
    middleFunctionCall(update, req, res, tableName)
  );
  router.post("/", (req, res) =>
    middleFunctionCall(create, req, res, tableName)
  );
  return router;
};

module.exports = {
  CommonRoutes,
};
