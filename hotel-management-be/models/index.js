const { LocalDB, ServerDB } = require("../config/dbCOnfig");
const initModels = require("./init-models");

module.exports = {
  db: initModels(LocalDB()),
  serverDB: initModels(ServerDB()),
};
