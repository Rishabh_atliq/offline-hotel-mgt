var DataTypes = require("sequelize").DataTypes;
var _tbl_customer = require("./tbl_customer");
var _tbl_hotel = require("./tbl_hotel");
var _tbl_reservation = require("./tbl_reservation");
var _tbl_reservation_details = require("./tbl_reservation_details");
var _tbl_role = require("./tbl_role");
var _tbl_room = require("./tbl_room");
var _tbl_room_type = require("./tbl_room_type");
var _tbl_user = require("./tbl_user");

function initModels(sequelize) {
  var tbl_customer = _tbl_customer(sequelize, DataTypes);
  var tbl_hotel = _tbl_hotel(sequelize, DataTypes);
  var tbl_reservation = _tbl_reservation(sequelize, DataTypes);
  var tbl_reservation_details = _tbl_reservation_details(sequelize, DataTypes);
  var tbl_role = _tbl_role(sequelize, DataTypes);
  var tbl_room = _tbl_room(sequelize, DataTypes);
  var tbl_room_type = _tbl_room_type(sequelize, DataTypes);
  var tbl_user = _tbl_user(sequelize, DataTypes);

  tbl_reservation.belongsTo(tbl_customer, { as: "customer", foreignKey: "customerID"});
  tbl_customer.hasMany(tbl_reservation, { as: "tbl_reservations", foreignKey: "customerID"});
  tbl_reservation.belongsTo(tbl_hotel, { as: "hotel", foreignKey: "hotelId"});
  tbl_hotel.hasMany(tbl_reservation, { as: "tbl_reservations", foreignKey: "hotelId"});
  tbl_user.belongsTo(tbl_hotel, { as: "hotel", foreignKey: "hotelId"});
  tbl_hotel.hasMany(tbl_user, { as: "tbl_users", foreignKey: "hotelId"});
  tbl_reservation_details.belongsTo(tbl_reservation, { as: "reservation", foreignKey: "reservationID"});
  tbl_reservation.hasMany(tbl_reservation_details, { as: "tbl_reservation_details", foreignKey: "reservationID"});
  tbl_user.belongsTo(tbl_role, { as: "role", foreignKey: "roleId"});
  tbl_role.hasMany(tbl_user, { as: "tbl_users", foreignKey: "roleId"});
  tbl_reservation_details.belongsTo(tbl_room, { as: "room", foreignKey: "roomID"});
  tbl_room.hasMany(tbl_reservation_details, { as: "tbl_reservation_details", foreignKey: "roomID"});
  tbl_room.belongsTo(tbl_room_type, { as: "type", foreignKey: "typeId"});
  tbl_room_type.hasMany(tbl_room, { as: "tbl_rooms", foreignKey: "typeId"});
  // sequelize.sync()
  return {
    tbl_customer,
    tbl_hotel,
    tbl_reservation,
    tbl_reservation_details,
    tbl_role,
    tbl_room,
    tbl_room_type,
    tbl_user,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
