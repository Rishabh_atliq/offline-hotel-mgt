const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tbl_reservation_details', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    reservationID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'tbl_reservation',
        key: 'id'
      }
    },
    roomID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'tbl_room',
        key: 'id'
      }
    },
    noOfPerson: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    is_active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: 1
    },
    is_delete: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: 0
    },
    is_sync: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'tbl_reservation_details',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "detail_reservation_id",
        using: "BTREE",
        fields: [
          { name: "reservationID" },
        ]
      },
      {
        name: "detail_room_id",
        using: "BTREE",
        fields: [
          { name: "roomID" },
        ]
      },
    ]
  });
};
