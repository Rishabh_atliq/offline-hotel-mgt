import logo from "./logo.svg";
import "./App.css";
import Router from "./routes";
import "bootstrap/dist/css/bootstrap.min.css";
import { DataProvider } from "./context/data-service";

function App() {
  return (
    <div className="App">
      <DataProvider>
        <Router />
      </DataProvider>
    </div>
  );
}

export default App;
