import { useContext, useEffect } from "react";
import { Routes, Route, BrowserRouter } from "react-router-dom";
import NavBar from "./components/nav-bar";
import { DataActions, DataContext } from "./context/data-service";
import Customer from "./pages/Customer";
import Home from "./pages/Home";
import User from "./pages/user";
import { getData } from "./services/http";
const Router = () => {
  const { dispatch } = useContext(DataContext);
  const getDatas = async () => {
    try {
      let result = await getData("role");
      dispatch(DataActions.setRole(result));
      result = await getData("hotel");
      dispatch(DataActions.setHotel(result));
    } catch (e) {}
  };
  useEffect(() => {
    getDatas();
  }, []);
  return (
    <>
      <div className="container-fluid">
        <BrowserRouter>
          <NavBar />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/user" element={<User />} />
            <Route path="/customer" element={<Customer />} />
          </Routes>
        </BrowserRouter>
      </div>
    </>
  );
};

export default Router;
