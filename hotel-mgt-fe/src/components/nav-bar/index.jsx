import { Link } from "react-router-dom";

const NavBar = () => {
  const menuItems = [
    {
      label: "Home",
      route: "/",
    },
    {
      label: "User",
      route: "/user",
    },
    {
      label: "Customer",
      route: "/customer",
    },
  ];
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <a className="navbar-brand" href="#">
        Hotel Management
      </a>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav mr-auto">
          {menuItems.map((item, index) => (
            <li className="nav-item active">
              <Link to={item.route} className="nav-link" key={"menu-" + index}>
                {item.label}
              </Link>
            </li>
          ))}
        </ul>
      </div>
    </nav>
  );
};

export default NavBar;
