import { useEffect } from "react";
import { useForm } from "react-hook-form";
const TableModal = ({
  show,
  handleClose,
  title,
  onSubmit,
  onEdit,
  cols,
  row,
}) => {
  console.log(row);
  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm({
    shouldFocusError: true,
    shouldUnregister: true,
    reValidateMode: "onChange",
  });
  useEffect(() => {
    if (row) {
      cols.forEach(({ field }) => setValue(field, row[field]));
    }
  }, [row]);
  return show ? (
    <div>
      <div
        className="modal fade show d-block"
        id="exampleModal"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <form
              onSubmit={handleSubmit((data) => {
                row ? onEdit(row, data) : onSubmit(data);
                handleClose();
              })}
            >
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  {title}
                </h5>
                <button
                  type="button"
                  className="close btn btn-light"
                  data-dismiss="modal"
                  aria-label="Close"
                  onClick={handleClose}
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                {cols &&
                  cols.map((col) => (
                    <div class="mb-3" key={"field_" + col.field}>
                      <label for={col.field} class="form-label d-flex">
                        {col.label}
                      </label>
                      {col.type === "select" ? (
                        <>
                          <select
                            className={`form-control ${
                              errors[col.field] ? "is-invalid" : ""
                            }`}
                            id={col.field}
                            {...register(col.field, { required: true })}
                          >
                            <option value={""}>SELECT {col.label}</option>
                            {col?.options?.map((op) => (
                              <option
                                value={op.value}
                                key={`op-${col.label}-${op.value}`}
                              >
                                {op.label}
                              </option>
                            ))}
                          </select>
                        </>
                      ) : (
                        <>
                          <input
                            className={`form-control ${
                              errors[col.field] ? "is-invalid" : ""
                            }`}
                            id={col.field}
                            type={col.type || "text"}
                            {...register(col.field, { required: true })}
                          />
                        </>
                      )}
                      <div className="invalid-feedback">
                        {errors[col.field]?.message}
                      </div>
                    </div>
                  ))}
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                  onClick={handleClose}
                >
                  Close
                </button>
                <button type="submit" className="btn btn-primary">
                  Save changes
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div className="modal-backdrop fade show"></div>
    </div>
  ) : (
    <></>
  );
};
export default TableModal;
