import { useState } from "react";
import TableModal from "../modal";

const DataTable = ({ data, cols, title, onSubmit, onDelete, onEdit }) => {
  const [showAddModal, setAddModal] = useState({ show: false, title: "" });
  return (
    <div className="card card-body">
      <TableModal
        {...showAddModal}
        cols={cols}
        handleClose={() => setAddModal({ show: false, title: "" })}
        onSubmit={onSubmit}
        onEdit={(oldRow, data) => onEdit(oldRow, data)}
      />
      <div className="d-flex justify-content-between">
        <h1>{title}</h1>
        <div>
          <button
            className="btn btn-primary"
            onClick={() => setAddModal({ show: true, title: "Add " + title })}
          >
            Add
          </button>
        </div>
      </div>
      <table className="table">
        <thead>
          <tr>
            {cols &&
              cols.map &&
              cols.filter(({hideForTable}) => !hideForTable).map((col, index) => (
                <th key={"th-" + index}>{col.label}</th>
              ))}
          </tr>
        </thead>
        <tbody>
          {data &&
            data.map &&
            data.map((row, index) => (
              <tr key={"row_" + index}>
                {cols.map((col, index) => (
                  <td key={"td-" + index}>{row[col.field]}</td>
                ))}
                <td className="d-flex justify-content-around">
                  <button
                    className="btn btn-primary"
                    onClick={() =>
                      setAddModal({
                        show: true,
                        title: "Edit " + title,
                        row,
                      })
                    }
                  >
                    Edit
                  </button>
                  <button
                    className="btn btn-primary"
                    onClick={() => onDelete(row)}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
        </tbody>
      </table>
    </div>
  );
};
export default DataTable;
