import axios from "axios";
const baseURL = "http://localhost:8081/api/v1";
export const getData = async (tableName) => {
  const result = await axios.get(`${baseURL}/${tableName}/findAll`);
  return result && result.data && result.data.data;
};
export const getActiveData = async (tableName) => {
  const result = await axios.get(`${baseURL}/${tableName}`);
  return result && result.data && result.data.data;
};
export const create = (tableName, data) => {
  return axios.post(`${baseURL}/${tableName}`, { ...data });
};
export const update = (tableName, data, id) => {
  return axios.put(`${baseURL}/${tableName}/${id}`, { ...data });
};
export const deteleData = (tableName, id) => {
  return axios.delete(`${baseURL}/${tableName}/${id}`);
};
