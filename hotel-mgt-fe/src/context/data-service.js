import { createContext, useEffect, useReducer } from "react";
import { getValue, setValue } from "./persistent";

export const DataContext = createContext();
const initialState = {
  user: [],
  customer: [],
  hotel: [],
  role: [],
};
const type = {
  addCustomer: "DATA ADD CUSTOMER",
  addUser: "DATA ADD USER",
  setData: "DATA SetData",
  setHotel: "DATA SETHOTEL",
  setRole: "DATA SETROLE",
};
export const DataActions = {
  setData: (payload) => ({
    payload,
    type: type.setData,
  }),
  setCustomer: (payload) => ({
    payload,
    type: type.addCustomer,
  }),
  setUser: (payload) => ({
    payload,
    type: type.addUser,
  }),
  setHotel: (payload) => ({
    payload,
    type: type.setHotel,
  }),
  setRole: (payload) => ({
    payload,
    type: type.setRole,
  }),
};
const reducer = (state, action) => {
  switch (action.type) {
    case type.addCustomer:
      return {
        ...state,
        customer: action.payload,
      };
    case type.addUser:
      return {
        ...state,
        user: action.payload,
      };
    case type.setData:
      return {
        ...action.payload,
      };
    case type.setRole:
      return {
        ...state,
        role: action.payload,
      };
    case type.setHotel:
      return {
        ...state,
        hotel: action.payload,
      };
    default:
      return state;
  }
};
const logger = async (action) => {
  await setValue(action);
};
const useReducerWithMiddleware = (reducer, initialState, middlewareFn) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const dispatchWithMiddleware = (action) => {
    dispatch(action);
    middlewareFn(reducer(state, action));
  };

  return [state, dispatchWithMiddleware];
};
export const DataProvider = ({ children }) => {
  const [state, dispatch] = useReducerWithMiddleware(
    reducer,
    initialState,
    logger
  );
  useEffect(() => {
    const getLevelData = async () => {
      const data = await getValue(initialState);
      console.log(data);
      dispatch(DataActions.setData(data));
    };
    getLevelData();
  }, []);
  return (
    <DataContext.Provider
      value={{
        state,
        dispatch,
      }}
    >
      {children}
    </DataContext.Provider>
  );
};
