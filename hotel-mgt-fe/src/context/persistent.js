const { Level } = require("level");
const db = new Level("Hotel-mgt", { valueEncoding: "json" });

export const setValue = async (value) => {
  return await db.put("data", value);
};
export const getValue = async (initialState) => {
  try {
    const data = await db.get("data");
    return data || initialState;
  } catch (e) {
    setValue(initialState);
    return await getValue();
  }
};
