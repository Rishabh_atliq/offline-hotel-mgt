import { useContext, useEffect, useState } from "react";
import DataTable from "../../components/data-table";
import { DataActions, DataContext } from "../../context/data-service";
import { getActiveData, create, update, deteleData } from "../../services/http";

const User = () => {
  const { state, dispatch } = useContext(DataContext);
  const cols = [
    {
      label: "First Name",
      field: "firstName",
    },
    {
      label: "Last Name",
      field: "lastName",
    },
    {
      label: "Email",
      field: "email",
    },
    {
      label: "Password",
      hideForTable: true,
      field: "password",
      type: "password",
    },
    {
      label: "Hotel",
      hideForTable: true,
      type: "select",
      options: state?.hotel?.map((hotel) => ({
        value: hotel.id,
        label: hotel.name,
      })),
      field: "hotelId",
    },
    {
      label: "Role",
      hideForTable: true,
      type: "select",
      options: state?.role?.map((role) => ({
        value: role.id,
        label: role.roleName,
      })),
      field: "roleId",
    },
  ];
  const data = state.user;
  const getRows = async () => {
    try {
      const result = await getActiveData("user");
      if (result) {
        dispatch(DataActions.setUser(result));
      }
    } catch (e) {}
  };
  useEffect(() => {
    getRows();
  }, []);
  const onEdit = async (data, newData) => {
    console.log(data, newData);
    data = { ...data, ...newData };
    await update("user", data, data.id);
    getRows();
  };
  const onDelete = async (data) => {
    await deteleData("user", data.id);
    getRows();
  };
  const onSubmit = async (data) => {
    await create("user", data);
    getRows();
  };
  return (
    <>
      <div>
        <DataTable
          data={data}
          cols={cols}
          title="User"
          onSubmit={onSubmit}
          onDelete={onDelete}
          onEdit={onEdit}
        />
      </div>
    </>
  );
};
export default User;
