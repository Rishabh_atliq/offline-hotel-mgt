import { useContext, useEffect, useState } from "react";
import DataTable from "../../components/data-table";
import { DataActions, DataContext } from "../../context/data-service";
import { getActiveData } from "../../services/http";

const Customer = () => {
  const { state, dispatch } = useContext(DataContext);
  const cols = [
    {
      label: "First Name",
      field: "firstName",
    },
    {
      label: "Last Name",
      field: "lastName",
    },
    {
      label: "Phone Number",
      field: "phno",
    },
    {
      label: "Address",
      field: "address",
    },
  ];
  const data = state.customer;
  const getRows = async () => {
    try {
      const result = await getActiveData("customer");
      if (result) {
        dispatch(DataActions.setCustomer(result));
      }
    } catch (e) {}
  };
  useEffect(() => {
    getRows();
  }, []);
  const onEdit = (data, newData) => {
    console.log(data, newData);
  };
  const onDelete = () => {};
  const onSubmit = (data) => {
    console.log(data);
  };
  return (
    <>
      <div>
        <DataTable
          data={data}
          cols={cols}
          title="Customer"
          onSubmit={onSubmit}
          onDelete={onDelete}
          onEdit={onEdit}
        />
      </div>
    </>
  );
};
export default Customer;
