import { useEffect, useState } from "react";
import { getData } from "../../services/http";

const Home = () => {
  const [data, setData] = useState([]);
  const getData = async () => {
    try {
      const result = await getData("user");
      if (result && result.data) {
        setData(result.data.data);
      }
    } catch (e) {}
  };
  useEffect(() => {
    getData();
  }, []);
  const onEdit = () => {};
  const onDelete = () => {};
  const rows = data.map((row) => ({ ...row, onEdit: onEdit() }));
  return (
    <>
      <div>Home</div>
    </>
  );
};
export default Home;
